package br.furb;

public class Acl {

	private Usuario u;
	private CrudUsuario crud = new CrudUsuario();
	
	public Acl roleTo(String login) {
		try {
			u = crud.buscar(login);			
		} catch (NaoAchouUsuario e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}

	public void addVisualizarRole() {
		if (u == null) {
			try {
				throw new NaoAchouUsuario("n�o achei seu usu�rio");
			} catch (NaoAchouUsuario e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		u.addRole(Role.VISUALIZAR);
		crud.atualizarRole(u);
	}
	
	public void addEnviarRole() {
		if (u == null) {
			try {
				throw new NaoAchouUsuario("n�o achei seu usu�rio");
			} catch (NaoAchouUsuario e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		u.addRole(Role.ENVIAR);
	}
	
}
