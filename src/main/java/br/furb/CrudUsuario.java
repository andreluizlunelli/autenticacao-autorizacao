package br.furb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class CrudUsuario {

	public static File usuariosFile = new File("usuarios.json");
	
	public CrudUsuario() {
		criarFile();
	}
	
	private File criarFile() {
		if ( ! usuariosFile.exists()) {
			try {
				usuariosFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
		return usuariosFile;
	}
	
	public void criar(Usuario usuario) {
		JSONParser parser = new JSONParser();
		JSONArray ja = null;		
		if (usuariosFile.exists()) {
			if (usuariosFile.length() > 0) {
				try {
					ja = (JSONArray) parser.parse(new FileReader(usuariosFile));
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				ja = new JSONArray();
			}			
		}	
		
		ja.add(usuario.toJson());
		
		PrintWriter writer;
		try {
			writer = new PrintWriter(usuariosFile);
			writer.print("");
			writer.close();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try (FileWriter file = new FileWriter(usuariosFile)) {
			file.write(ja.toJSONString());			
		} catch (IOException  e) {
			e.printStackTrace();
		}
	}

	public Usuario buscar(String login) throws NaoAchouUsuario {
		JSONParser parser = new JSONParser();
		JSONArray ja = null;		
		try {
			if (usuariosFile.exists() && usuariosFile.length() > 0) {
				ja = (JSONArray) parser.parse(new FileReader(usuariosFile));
			} else {
				throw new NaoAchouUsuario("n�o achou o usu�rio");
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JSONObject j = null;
		for (Object object : ja) {
			j = (JSONObject) object;
			if (login.equals((String) j.get("login"))) {											
				Usuario u = new Usuario((String) j.get("nome"), login, "1");
				u.setPass((String) j.get("pass"));
				u.setSalt(((Long) j.get("salt")).intValue());
				return u;
			}
		}
		throw new NaoAchouUsuario("n�o achou o usu�rio");
	}

	public void atualizarRole(Usuario u) {
		JSONParser parser = new JSONParser();
		JSONArray ja = null;		
		if (usuariosFile.exists()) {
			if (usuariosFile.length() > 0) {
				try {
					ja = (JSONArray) parser.parse(new FileReader(usuariosFile));
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				ja = new JSONArray();
			}			
		}	
		
		ja.add(u.toJson());
		
		PrintWriter writer;
		try {
			writer = new PrintWriter(usuariosFile);
			writer.print("");
			writer.close();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try (FileWriter file = new FileWriter(usuariosFile)) {
			file.write(ja.toJSONString());			
		} catch (IOException  e) {
			e.printStackTrace();
		}
	}

		
}
