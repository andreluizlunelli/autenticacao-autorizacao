package br.furb;

import java.io.Serializable;

public enum Role implements Serializable {
	VISUALIZAR, ENVIAR
}
