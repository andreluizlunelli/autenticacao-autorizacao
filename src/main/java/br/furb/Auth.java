package br.furb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Auth {

	private CrudUsuario crud = null;	
	
	public Auth() {
		super();		
		crud = new CrudUsuario();
	}		

	public boolean auth(String login, String pass) {
		Usuario u = null;
		try {
			u = crud.buscar(login);
		} catch (NaoAchouUsuario e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return u.samePass(pass);		
	}	


	
}
