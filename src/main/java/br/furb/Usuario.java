package br.furb;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;

public class Usuario implements Serializable {
	private String nome;
	private String login;
	private String pass;
	private int salt;	
	private Set<Role> roles = new HashSet<>();	
	
	public Usuario(String nome, String login, String pass) {
		super();		
				
		this.salt = new SecureRandom().nextInt();
		
		if (this.salt < 0) {
			this.salt = this.salt * -1;
		}
		
		this.nome = nome;
		this.login = login;
		this.pass = criarPass(pass);
	}
	
	public boolean samePass(Usuario u) {
		return samePass(u.getPass());
	}
	
	public boolean samePass(String pass) {
		HashCode hashString = Hashing.sha256().hashString(pass+salt, StandardCharsets.UTF_8);					
	    return hashString.toString().equals(this.pass);
	}

	private String criarPass(String pass) {		
		HashCode hashString = Hashing.sha256().hashString(pass+salt, StandardCharsets.UTF_8);					
	    return hashString.toString();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public JSONObject toJson() {
		JSONObject j = new JSONObject();
		j.put("nome", this.getNome());
		j.put("login", this.getLogin());
		j.put("pass", this.getPass());
		j.put("salt", this.getSalt());
		JSONArray ja = new JSONArray();
		ja.addAll(this.getRoles());
		j.put("roles", ja);
		return j;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public int getSalt() {
		return salt;
	}

	public void setSalt(int salt) {
		this.salt = salt;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	public void addRole(Role role) {
		this.roles.add(role);
	}

}
