package br.furb.test;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import br.furb.Acl;
import br.furb.CrudUsuario;
import br.furb.NaoAchouUsuario;
import br.furb.Role;
import br.furb.Usuario;

public class ACLTest {

	@Test
	public void test01() throws NaoAchouUsuario {
		CrudUsuario crud = new CrudUsuario();
		crud.criar(new Usuario("Andre", "andre", "123456"));
		
		Acl acl = new Acl();
		acl.roleTo("andre").addVisualizarRole();
		
		Usuario u = crud.buscar("andre");
		Assert.assertTrue(u.getRoles().contains(Role.VISUALIZAR));
	}
	
}

