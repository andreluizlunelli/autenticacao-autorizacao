package br.furb.test;

import org.junit.Test;

import br.furb.Auth;
import br.furb.CrudUsuario;
import br.furb.Usuario;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Random;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Assert;

public class AutenticarTest {

	@Test
	public void test01() {
		PrintWriter writer;
		try {
			writer = new PrintWriter(CrudUsuario.usuariosFile);
			writer.print("");
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// cria usu�rio
		CrudUsuario crud = new CrudUsuario();
		crud.criar(new Usuario("andre", "teste", "123456"));

		// testa se o login:teste e senha:123456 � um usu�rio v�lido
		Auth a = new Auth();
		Assert.assertTrue(a.auth("teste", "123456"));
	}
	
	

}
