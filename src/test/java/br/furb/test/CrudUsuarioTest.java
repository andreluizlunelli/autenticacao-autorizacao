package br.furb.test;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.furb.CrudUsuario;
import br.furb.NaoAchouUsuario;
import br.furb.Role;
import br.furb.Usuario;

public class CrudUsuarioTest {
	
	@Before
	public void before() {
		PrintWriter writer;
		try {
			writer = new PrintWriter(CrudUsuario.usuariosFile);
			writer.print("");
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void test01() {
		CrudUsuario crud = new CrudUsuario();
		crud.criar(new Usuario("andre", "andre", "123456"));
		Usuario u = null;
		try {
			u = crud.buscar("andre");
		} catch (NaoAchouUsuario e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Assert.assertEquals("andre", u.getNome());
		Assert.assertEquals("andre", u.getLogin());
		Assert.assertEquals("123456", u.getPass());	
	}
		
	//@Test
	public void test02() {
		Usuario usuario = new Usuario("andre", "andre", "123456");
		JSONObject j = usuario.toJson();
		Assert.assertEquals("{\"pass\":\"123456\",\"nome\":\"andre\",\"login\":\"andre\"}", j);
	}
	
	@Test
	public void addrole() throws NaoAchouUsuario {
		CrudUsuario crud = new CrudUsuario();
		crud.criar(new Usuario("andre", "andre", "123456"));
		Usuario u = crud.buscar("andre");
		
		u.addRole(Role.VISUALIZAR);			
		
		crud.atualizarRole(u);
		
		Usuario u2 = crud.buscar("andre");
		Assert.assertTrue(u2.getRoles().contains(Role.VISUALIZAR));
	}
	
}
